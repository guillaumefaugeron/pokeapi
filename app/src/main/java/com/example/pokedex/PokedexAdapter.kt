package com.example.pokedex

import android.content.Context
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.graphics.ColorUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pokedex.utils.PokemonUtils


class PokemonListAdapter(val context: Context, private val mList: List<PokemonsViewModel>) :
    RecyclerView.Adapter<PokemonListAdapter.ViewHolder>() {

    var onItemClick: ((PokemonsViewModel) -> Unit)? = null
    var onCheckboxClick: ((PokemonsViewModel) -> Unit)? = null
    private val pokeUtils = PokemonUtils()

    /**
     * Creates each views, one per element in data
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        // Inflates our layout to display data
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.recycler_view_design, parent, false)

        return ViewHolder(view)
    }

    /**
     * Binds the list items to a view so we can add our data to it
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val pokemonsViewModel = mList[position]

        // Loads pokemon sprite
        Glide.with(holder.pokemonSprite).load(pokemonsViewModel.pokemon?.sprites?.frontDefault)
            .into(holder.pokemonSprite)

        // Sets pokemon name, filter to load it in French
        holder.pokemonName.text =
            pokemonsViewModel.pokemon?.species?.names?.first { it.language.id == 5 }?.name

        // Sets pokedex number for the current pokemon, i.e its id in the API
        holder.pokemonNumber.text = "#".plus(pokemonsViewModel.pokemon?.id.toString())

        // This block handles display of pokemon type(s)
        val pokemonType0 = pokemonsViewModel.pokemon?.types?.get(0)?.type?.name
        var pokemonType1 = ""
        try {
            pokemonType1 = pokemonsViewModel.pokemon?.types?.get(1)?.type?.name.toString()
        } catch (e: IndexOutOfBoundsException) {
        }

        holder.pokemonType0.text =
            pokemonsViewModel.types?.first { it.name == pokemonType0 }?.names?.first { it.language.id == 5 }?.name.toString()
        holder.pokemonType0.background.colorFilter =
            BlendModeColorFilter(
                Color.parseColor(pokeUtils.getPokemonTypeColor(pokemonType0)),
                BlendMode.SRC_ATOP
            )

        // If Pokemon has multiple types
        if (pokemonType1 != "") {
            holder.pokemonType1.visibility = View.VISIBLE
            holder.pokemonType1.text =
                pokemonsViewModel.types?.first { it.name == pokemonType1 }?.names?.first { it.language.id == 5 }?.name.toString()
            holder.pokemonType1.background.colorFilter =
                BlendModeColorFilter(
                    Color.parseColor(pokeUtils.getPokemonTypeColor(pokemonType1)),
                    BlendMode.SRC_ATOP
                )
        } else {
            holder.pokemonType1.visibility = View.INVISIBLE
        }

        // Sets background color of Pokemon according to its main type (type 0)
        val layout: LinearLayout = holder.pokemonName.parent.parent as LinearLayout
        layout.setBackgroundColor(
            ColorUtils.blendARGB(
                Color.parseColor(
                    pokeUtils.getPokemonTypeColor(
                        pokemonType0
                    )
                ), Color.WHITE, 0.5f
            )
        )

        // Checkbox is automatically checked or not depending on capture status
        holder.pokemonCheckbox.isChecked = pokemonsViewModel.capturedStatus
    }

    /**
     * Returns the number of the items in the list
     */
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views used to add text, images and event listener
    inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val pokemonSprite: ImageView = itemView.findViewById(R.id.pokemonSprite)
        val pokemonName: TextView = itemView.findViewById(R.id.pokemonName)
        val pokemonType0: TextView = itemView.findViewById(R.id.pokemonType0)
        val pokemonType1: TextView = itemView.findViewById(R.id.pokemonType1)
        val pokemonNumber: TextView = itemView.findViewById(R.id.pokemonNumber)
        val pokemonCheckbox: CheckBox = itemView.findViewById(R.id.pokemonCheckbox)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(mList[adapterPosition])
            }
            pokemonCheckbox.setOnClickListener {
                onCheckboxClick?.invoke(mList[adapterPosition])
            }
        }
    }
}