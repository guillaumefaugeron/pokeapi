package com.example.pokedex

import com.example.pokedex.model.PokemonDto
import me.sargunvohra.lib.pokekotlin.model.Type

data class PokemonsViewModel(
    val pokemon: PokemonDto?,
    val types: List<Type>?,
    var capturedStatus: Boolean
) {
}