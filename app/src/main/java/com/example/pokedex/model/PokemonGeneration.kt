package com.example.pokedex.model

import java.io.InputStream

class PokemonGeneration(
    var number: String? = null,
    var image: InputStream? = null
)