package com.example.pokedex.model;

import java.util.List;

public class User {
    String username;
    List<Integer> capturedPokemon;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Integer> getCapturedPokemon() {
        return capturedPokemon;
    }

    public void setCapturedPokemon(List<Integer> capturedPokemon) {
        this.capturedPokemon = capturedPokemon;
    }
}
