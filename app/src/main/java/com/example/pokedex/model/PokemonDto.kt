package com.example.pokedex.model

import me.sargunvohra.lib.pokekotlin.model.*

data class PokemonDto(
    var id: Int,
    var name: String,
    var baseExperience: Int,
    var height: Int,
    var isDefault: Boolean,
    var order: Int,
    var weight: Int,
    var species: PokemonSpecies?,
    var abilities: List<PokemonAbility>,
    var forms: List<NamedApiResource>,
    var gameIndices: List<VersionGameIndex>,
    var heldItems: List<PokemonHeldItem>,
    var moves: List<PokemonMove>,
    var stats: List<PokemonStat>,
    var types: List<PokemonType>,
    var sprites: PokemonSprites
)
