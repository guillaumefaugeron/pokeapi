package com.example.pokedex

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.ImageView
import android.graphics.BitmapFactory

import java.io.IOException


class PokemonGenerationAdapter(
    c: Context,
    private val pokemonGenerations: ArrayList<PokemonGenerationViewModel>
) : BaseAdapter() {

    var context: Context? = c
    private val layoutInflater: LayoutInflater = LayoutInflater.from(c)


    override fun getCount(): Int {
        return pokemonGenerations.size
    }

    override fun getItem(p0: Int): Any {
        return pokemonGenerations[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    /**
     * Handles creation of card views when opening generations filter dialog
     */
    override fun getView(position: Int, defaultConvertView: View?, parent: ViewGroup?): View? {
        var convertView = defaultConvertView
        val pokemonGeneration: PokemonGenerationViewModel = this.pokemonGenerations[position]
        val holder: ViewHolderItem

        // Prepares the view if null
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.activity_poke_list_generations_grid, null)
            holder = ViewHolderItem()
            holder.pokemonGenerationImage =
                convertView.findViewById<View>(R.id.pokemonGenerationImage) as ImageView
            holder.pokemonGenerationText =
                convertView.findViewById<View>(R.id.pokemonGenerationText) as TextView
            convertView.tag = holder
        } else {
            holder = convertView.tag as ViewHolderItem
        }

        // Loads generation illustration from raw folder, then resets bitmap buffer, as it may
        // cause bugs if data is still present
        val pokemonGenerationBitmap =
            BitmapFactory.decodeStream(pokemonGeneration.pokemonGeneration?.image)
        try {
            pokemonGeneration.pokemonGeneration?.image?.reset()
        } catch (e: IOException) {
            return null
        }

        // Sets text and image
        holder.pokemonGenerationText?.text = pokemonGeneration.pokemonGeneration?.number
        holder.pokemonGenerationImage?.setImageBitmap(pokemonGenerationBitmap)
        return convertView
    }

    // Contains views information about the pokemon generation
    internal class ViewHolderItem {
        var pokemonGenerationText: TextView? = null
        var pokemonGenerationImage: ImageView? = null
    }
}