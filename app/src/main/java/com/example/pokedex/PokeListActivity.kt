package com.example.pokedex

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pokedex.model.PokemonDto
import com.example.pokedex.model.PokemonGeneration
import com.example.pokedex.model.User
import com.example.pokedex.service.PokeService
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.*
import me.sargunvohra.lib.pokekotlin.model.Type
import java.io.*
import java.net.UnknownHostException
import java.util.*
import java.util.concurrent.Executors
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

val threadPool = Executors.newSingleThreadExecutor()!!
val threadPoolDispatcher = threadPool.asCoroutineDispatcher()

class PokeListActivity : AppCompatActivity(), CoroutineScope by MainScope() {

    var adapter: PokemonListAdapter = PokemonListAdapter(this, ArrayList())
    private var data = ArrayList<PokemonsViewModel>()
    var isLoading: Boolean = false
    private val pokeService = PokeService()
    var currentOffset = 0
    private lateinit var types: List<Type>
    private val gson = Gson()
    private var pokemonsLoadedFromJson: List<PokemonDto>? = listOf()
    lateinit var pokemonCapturedIds: ArrayList<Int>
    var isLoadedFromApi: Boolean = false


    private lateinit var progressBar: ImageView
    private lateinit var loader: ConstraintLayout
    private lateinit var loaderContainer: ConstraintLayout
    private lateinit var errorContainer: ConstraintLayout
    private lateinit var fabContainer: ConstraintLayout

    // Floating Action Buttons Declaraion
    private lateinit var mainFab: FloatingActionButton
    private lateinit var cleanPokedexFab: FloatingActionButton
    private lateinit var selectGenerationFab: FloatingActionButton
    private lateinit var editUsernameFab: FloatingActionButton

    // Floating Action Buttons Texts Declaraion
    private lateinit var cleanPokedexEntriesText: TextView
    private lateinit var selectGenerationText: TextView
    private lateinit var editUsernameText: TextView

    private var isAllFabsVisible: Boolean = false

    private val pokemonGenerationsOffset: ArrayList<Int> =
        ArrayList<Int>(listOf(0, 151, 251, 386, 493, 649, 721, 809))


    /**
     * Function to read file with a json array inside and map it to a list of Kotlin objects
     * Will throw exception if the file doesn't exist
     */
    private fun readDataArrayFromFileAndMapToObject(fileName: String): List<PokemonDto>? {
        var fileInputStream: FileInputStream? = null
        fileInputStream = openFileInput(fileName)
        val inputStreamReader = InputStreamReader(fileInputStream)
        val bufferedReader = BufferedReader(inputStreamReader)
        val stringBuilder: StringBuilder = StringBuilder()
        var text: String? = null
        while (run {
                text = bufferedReader.readLine()
                text
            } != null) {
            stringBuilder.append(text)
        }
        val data = stringBuilder.toString()

        val myDataType: java.lang.reflect.Type =
            object : TypeToken<Collection<PokemonDto?>?>() {}.type
        return gson.fromJson(data, myDataType)
    }

    /**
     * Saves a String to a file
     * If the file doesn't exist it will be created
     */
    private fun saveDataToNewFile(fileName: String, fileContent: String) {
        val fileOutputStream: FileOutputStream

        try {
            fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE)
            fileOutputStream.write(fileContent.toByteArray())
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Function to read file with a json object inside and map it to an Kotlin user object
     * Will throw if the file doesn't exist
     */
    private fun readDataArrayFromFileAndMapToObjectUser(fileName: String): User {
        var fileInputStream: FileInputStream? = null
        fileInputStream = openFileInput(fileName)
        val inputStreamReader = InputStreamReader(fileInputStream)
        val bufferedReader = BufferedReader(inputStreamReader)
        val stringBuilder: StringBuilder = StringBuilder()
        var text: String? = null
        while (run {
                text = bufferedReader.readLine()
                text
            } != null) {
            stringBuilder.append(text)
        }
        val data = stringBuilder.toString()
        val gson = Gson()
        return gson.fromJson(data, User::class.java) as User
    }

    /**
     * Retrieves every captured Pokemon Ids from JSON
     */
    private fun getAllCapturedPokemonDtoId(): List<Int> {
        return try {
            val user = readDataArrayFromFileAndMapToObjectUser("user.json")
            user.capturedPokemon.toList()
        } catch (e: FileNotFoundException) {

            listOf<Int>()
        }
    }

    /**
     * Adds a Pokemon ID to the captured lists and saves it to JSON
     */
    private fun saveOneCapturedPokemonDtoId(id: Int?) {
        try {
            val user = readDataArrayFromFileAndMapToObjectUser("user.json")
            user.capturedPokemon.add(id)
            saveDataToNewFile("user.json", gson.toJson(user))
            if (id != null) {
                pokemonCapturedIds.add(id)
            }
        } catch (e: FileNotFoundException) {
            print(e)
        }
    }

    /**
     * Removes a Pokemon ID from the captured lists and saves it to JSON
     */
    private fun deleteOneCapturedPokemonDtoId(id: Int?) {
        try {
            val user = readDataArrayFromFileAndMapToObjectUser("user.json")
            user.capturedPokemon.remove(id)
            saveDataToNewFile("user.json", gson.toJson(user))
            pokemonCapturedIds.remove(id)
        } catch (e: FileNotFoundException) {
            print(e)
        }
    }

    /**
     * Removes every Pokemon IDs from the captured lists and saves it to JSON
     */
    private fun deleteEveryCapturedPokemonDtoIds() {
        try {
            val user = readDataArrayFromFileAndMapToObjectUser("user.json")
            user.capturedPokemon = listOf()
            saveDataToNewFile("user.json", gson.toJson(user))
            pokemonCapturedIds = ArrayList<Int>()
        } catch (e: FileNotFoundException) {
            print(e)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        // In all cases we have an user in these conditions
        val user = readDataArrayFromFileAndMapToObjectUser("user.json")
        supportActionBar?.title = "Pokédex de " + user.username
        pokemonCapturedIds = ArrayList<Int>(getAllCapturedPokemonDtoId())

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_poke_list)

        loader = findViewById<ConstraintLayout>(R.id.pokemonListLoader)
        loaderContainer = findViewById<ConstraintLayout>(R.id.pokemonListLoaderContainer)
        errorContainer = findViewById<ConstraintLayout>(R.id.pokemonErrorContainer)
        fabContainer = findViewById<ConstraintLayout>(R.id.pokemonListFABContainer)

        Glide.with(this).load(R.raw.loader)
            .into(findViewById<ImageView>(R.id.pokemonListLoaderImage))

        Glide.with(this).load(R.raw.error)
            .into(findViewById<ImageView>(R.id.pokemonErrorImage))

        loaderContainer.visibility = View.VISIBLE

        progressBar = findViewById<ImageView>(R.id.pokemonListProgressBar)

        Glide.with(this).load(R.raw.loader).into(progressBar)

        val recyclerview = findViewById<RecyclerView>(R.id.pokemonRecyclerView)
        recyclerview.layoutManager = LinearLayoutManager(this)

        adapter = PokemonListAdapter(this, data)
        recyclerview.adapter = adapter


        launch {
            try {
                // Trying to retrieve every pokemons from file
                pokemonsLoadedFromJson = readDataArrayFromFileAndMapToObject("pokemons.json")
                if (pokemonsLoadedFromJson?.size!! > 800) {
                    isLoadedFromApi = false
                }
            } catch (e: FileNotFoundException) {
                // The pokemons will be retrieved by the API this time
                isLoadedFromApi = true
            }
            try {
                types = pokeService.getTypes()
                if (isLoadedFromApi) {
                    // Launch the writing of the JSON file in background
                    // the user can use the app normally but should not close it
                    fillRecyclerViewFromApi(currentOffset)
                } else {
                    pokemonsLoadedFromJson?.forEach {
                        val capturedStatus = pokemonCapturedIds.contains(it.id)
                        data.add(
                            PokemonsViewModel(it, types, capturedStatus)
                        )
                    }
                }
                adapter.notifyDataSetChanged()
                isLoading = false
                hideProgressView()
                loaderContainer.visibility = View.GONE
                fabContainer.visibility = View.VISIBLE
                currentOffset = data.lastIndex - 20
                if (isLoadedFromApi) {
                    cachePokemonsToJSON()
                }
            } catch (e: UnknownHostException) {
                isLoading = false
                loaderContainer.visibility = View.GONE
                errorContainer.visibility = View.VISIBLE
                val errorText: TextView = findViewById(R.id.pokemonErrorText)
                errorText.text = getString(R.string.no_internet)
            } catch (e: Exception) {
                isLoading = false
                loaderContainer.visibility = View.GONE
                errorContainer.visibility = View.VISIBLE
                val errorText: TextView = findViewById(R.id.pokemonErrorText)
                errorText.text = e.toString()
            }
        }

        // Handles the refresh of recyclerview when reaching bottom of the list
        recyclerview.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                // We only load new data if it is loaded from API, and if no other loading
                // is in progress
                if (!recyclerView.canScrollVertically(1) && !isLoading && isLoadedFromApi) {
                    isLoading = true
                    showProgressView()
                    // We load new Pokemons 20 by 20
                    currentOffset += 20
                    launch {
                        fillRecyclerViewFromApi(currentOffset)
                        adapter.notifyDataSetChanged()
                        isLoading = false
                        hideProgressView()
                    }
                }
            }
        })

        // Handles click on a pokemon inside the list, then starts the proper details activity
        adapter.onItemClick = { itemViewHolder ->
            val intent = Intent(this, PokeDetailsActivity::class.java).apply {
                putExtra("EXTRA_POKEMON_ID", itemViewHolder.pokemon?.id)
            }
            startActivity(intent)
        }

        // Handles click on pokemon checkbox, save or remove its id from captured list according
        // to its previous status
        adapter.onCheckboxClick = { itemViewHolder ->
            val pokemonId = itemViewHolder.pokemon?.id
            if (pokemonCapturedIds.contains(pokemonId)) {
                deleteOneCapturedPokemonDtoId(pokemonId)
                itemViewHolder.capturedStatus = false
            } else {
                saveOneCapturedPokemonDtoId(pokemonId)
                itemViewHolder.capturedStatus = true
            }
        }


        // Registers every FABs with their IDs
        mainFab = findViewById(R.id.add_fab)
        cleanPokedexFab = findViewById(R.id.pokemonListFABCleanPokedex)
        selectGenerationFab = findViewById(R.id.pokemonListFilterGenerationFAB)
        editUsernameFab = findViewById(R.id.pokemonListEditUsernameFAB)

        // Registers the text for each FABs
        cleanPokedexEntriesText = findViewById(R.id.pokemonListFABCleanPokedexText)
        selectGenerationText = findViewById(R.id.pokemonListFilterGenerationFABText)
        editUsernameText = findViewById(R.id.pokemonListEditUsernameFABText)

        // Hides submenu by default
        hideFab()

        // Displays submenu when pressing main FAB
        mainFab.setOnClickListener {
            isAllFabsVisible = if (!isAllFabsVisible) {
                showFab()
                true
            } else {
                hideFab()
                false
            }
        }

        // Sets event listeners for each action button
        cleanPokedexFab.setOnClickListener {
            showPokedexRemovalDialog()
            hideFab()
        }

        selectGenerationFab.setOnClickListener {
            showPokemonGenerationAlertDialog()
            hideFab()
        }

        editUsernameFab.setOnClickListener {
            showUsernameEditDialog()
            hideFab()
        }
    }

    // Prevents going back to MainActivity
    override fun onBackPressed() {

    }

    /**
     * Handles display of the Pokemon generation filter
     */
    private fun showPokemonGenerationAlertDialog() {
        val customDialog = AlertDialog.Builder(this)
        val dialog: AlertDialog = customDialog.create()
        // Prepare grid view
        val gridView = GridView(this)
        val mList: ArrayList<Int> = ArrayList()
        for (i in 1..8) {
            mList.add(i)
        }
        val pokemonGenerationsData: ArrayList<PokemonGenerationViewModel> = getGenerationsList()

        gridView.adapter = PokemonGenerationAdapter(this, pokemonGenerationsData)
        gridView.numColumns = 4

        // This block handles Pokemons filtering, from API or JSON
        gridView.onItemClickListener =
            OnItemClickListener { _, _, _, id ->
                dialog.dismiss()
                currentOffset = pokemonGenerationsOffset[id.toInt()]

                // We clear previous data in adapter and display our loader
                data.clear()
                adapter.notifyDataSetChanged()
                loaderContainer.visibility = View.VISIBLE
                isLoading = true
                fabContainer.visibility = View.INVISIBLE

                // Starts a coroutine to load Pokemons data
                launch {
                    if (isLoadedFromApi) {
                        fillRecyclerViewFromApi(currentOffset)
                    } else {
                        data.clear()
                        pokemonsLoadedFromJson?.forEach {
                            if (it.id > currentOffset) {
                                val capturedStatus = pokemonCapturedIds.contains(it.id)
                                data.add(
                                    PokemonsViewModel(it, types, capturedStatus)
                                )
                            }
                        }
                    }
                    adapter.notifyDataSetChanged()
                    isLoading = false
                    loaderContainer.visibility = View.INVISIBLE
                    fabContainer.visibility = View.VISIBLE
                }
            }

        dialog.setView(gridView)
        dialog.setTitle("Génération à charger")
        dialog.show()
    }

    /**
     * Handles display of username edit dialog
     */
    private fun showUsernameEditDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Choisir le nouveau nom pour le dresseur")

        //Generates input to fill new name
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT

        // Adds auto focus to input and display keyboard
        input.requestFocus()
        val imm: InputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)

        builder.setView(input)

        builder.setPositiveButton("OK") { dialog, _ ->
            if (input.text.toString().isNotEmpty() && input.text.toString().length < 15) {
                updateNickname(input.text.toString())
                supportActionBar?.title = "Pokédex de " + input.text.toString()
                dialog.dismiss()
                Toast.makeText(
                    this@PokeListActivity,
                    "Votre nom a été modifié !",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                Toast.makeText(
                    this@PokeListActivity,
                    "Votre nom ne peut pas être vide ou excédé 15 caractères.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        builder.setNegativeButton(
            "Annuler"
        ) { dialog, _ ->
            dialog.dismiss()
        }
        builder.show()
    }

    /**
     * Handles display of Pokedex entries cleaning
     */
    private fun showPokedexRemovalDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setMessage("Voulez-vous vraiment vider les entrées du Pokédex ?")
            .setCancelable(false)
            .setPositiveButton("Oui") { dialog, _ ->
                dialog.cancel()
                deleteEveryCapturedPokemonDtoIds()
                // We keep the same data loaded in the recycler view, but we uncheck every checkbox
                data.forEach { c -> c.capturedStatus = false }
                adapter.notifyDataSetChanged()
                Toast.makeText(
                    this@PokeListActivity,
                    "Le Pokédéx a été vidé !",
                    Toast.LENGTH_SHORT
                ).show()
            }
            .setNegativeButton(
                "Non"
            ) { dialog, _ -> //  Action for 'NO' Button
                dialog.cancel()
            }

        val alert: AlertDialog = builder.create()
        alert.setTitle("Confirmation")
        alert.show()
    }

    /**
     * Displays submenu for FABs
     */
    private fun showFab() {
        cleanPokedexFab.show()
        selectGenerationFab.show()
        editUsernameFab.show()
        cleanPokedexEntriesText.visibility = View.VISIBLE
        selectGenerationText.visibility = View.VISIBLE
        editUsernameText.visibility = View.VISIBLE
        isAllFabsVisible = true
    }

    /**
     * Hides submenu for FABs
     */
    private fun hideFab() {
        cleanPokedexFab.hide()
        selectGenerationFab.hide()
        editUsernameFab.hide()
        cleanPokedexEntriesText.visibility = View.GONE
        selectGenerationText.visibility = View.GONE
        editUsernameText.visibility = View.GONE
        isAllFabsVisible = false
    }

    /**
     * Displays little loader at the bottom of the recyclerview
     */
    fun showProgressView() {
        progressBar.visibility = View.VISIBLE
    }

    /**
     * Hides little loader at the bottom of the recylerview
     */
    fun hideProgressView() {
        progressBar.visibility = View.INVISIBLE
    }

    /**
     * Loads the next 20 pokemons inside the recycler view
     */
    suspend fun fillRecyclerViewFromApi(offset: Int) = withContext(threadPoolDispatcher) {
        val pokemons = pokeService.getPokemons(20, offset)
        pokemons.forEach {
            val capturedStatus = pokemonCapturedIds.contains(it.id)
            data.add(
                PokemonsViewModel(it, types, capturedStatus)
            )
        }
    }

    /**
     * Saves new username to JSON
     */
    private fun updateNickname(username: String) {
        if (username.isNotEmpty() && username.length < 15) {
            val user = readDataArrayFromFileAndMapToObjectUser("user.json")
            user.username = username
            val gson = Gson()
            saveDataToNewFile("user.json", gson.toJson(user))
        }
    }

    /**
     * Saves every pokemon from the API to a JSON file
     */
    private suspend fun cachePokemonsToJSON() = withContext(threadPoolDispatcher) {
        var offset = 0
        lateinit var pokemonInOffset: List<PokemonDto>

        this@PokeListActivity.runOnUiThread {
            Toast.makeText(
                this@PokeListActivity,
                "Mise en cache des Pokémons en cours, merci de ne pas fermer l'application !",
                Toast.LENGTH_LONG
            ).show()
        }

        /**
         * Loop while there is pokémon to retrieve
         */
        while (offset < 898) {
            // On recupère les pokémons 20 par 20 pour ne pas surcharger
            pokemonInOffset = pokeService.getPokemons(20, offset)
            pokemonInOffset.forEach {
                it.gameIndices = emptyList()
                it.forms = emptyList()
                it.baseExperience = 0
                it.heldItems = emptyList()
                it.abilities = emptyList()
                it.moves = emptyList()
                pokemonsLoadedFromJson?.plus(it)
            }
            try {
                val jsonDatas = readDataArrayFromFileAndMapToObject("pokemons.json")
                if (jsonDatas != null) {
                    saveDataToNewFile(
                        "pokemons.json",
                        gson.toJson(jsonDatas.plus(pokemonInOffset))
                    )
                }
            } catch (e: FileNotFoundException) {
                saveDataToNewFile(
                    "pokemons.json",
                    gson.toJson(pokemonInOffset)
                )
            }
            offset += 20
            val progress = (offset.toDouble() / 898.0) * 100.0
            this@PokeListActivity.runOnUiThread {
                Toast.makeText(
                    this@PokeListActivity,
                    "Mise en cache des Pokémons ${progress.roundToInt()}%",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
        this@PokeListActivity.runOnUiThread {
            Toast.makeText(
                this@PokeListActivity,
                "Mise en cache des Pokémons terminée ! Vous pouvez désormais relancer l'application pour un chargement instantané.",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    /**
     * Function to retrieve every pokemon generations objects
     */
    private fun getGenerationsList(): ArrayList<PokemonGenerationViewModel> {
        val pokemonGenerationsData: ArrayList<PokemonGenerationViewModel> = ArrayList()
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "1ère",
                    resources.openRawResource(R.raw.mewtwo)
                )
            )
        )
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "2ème",
                    resources.openRawResource(R.raw.ho_oh)
                )
            )
        )
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "3ème",
                    resources.openRawResource(R.raw.rayquaza)
                )
            )
        )
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "4ème",
                    resources.openRawResource(R.raw.dialga)
                )
            )
        )
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "5ème",
                    resources.openRawResource(R.raw.zekrom)
                )
            )
        )
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "6ème",
                    resources.openRawResource(R.raw.zygarde)
                )
            )
        )
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "7ème",
                    resources.openRawResource(R.raw.solgaleo)
                )
            )
        )
        pokemonGenerationsData.add(
            PokemonGenerationViewModel(
                PokemonGeneration(
                    "8ème",
                    resources.openRawResource(R.raw.zacian)
                )
            )
        )
        return pokemonGenerationsData
    }
}