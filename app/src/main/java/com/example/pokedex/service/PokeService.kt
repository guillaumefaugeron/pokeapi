package com.example.pokedex.service

import com.example.pokedex.model.PokemonDto
import com.example.pokedex.utils.PokemonUtils
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient
import me.sargunvohra.lib.pokekotlin.model.*
import java.io.*


class PokeService {
    private val pokeApi = PokeApiClient()
    private val pokemonUtils = PokemonUtils()

    /**
     * Creates a coroutine that returns pokemon details from its id
     */
    suspend fun getPokemon(pokemonId: Int): PokemonDto = withContext(IO) {
        val pokemon = pokemonUtils.convertPokemonToDto(pokeApi.getPokemon(pokemonId))
        pokemon.species = pokeApi.getPokemonSpecies(pokemonId)
        return@withContext pokemon
    }

    /**
     * Creates a coroutine that returns every Pokemon types
     */
    suspend fun getTypes(): List<Type> = withContext(IO) {
        val typesList: ArrayList<Type> = ArrayList()
        pokeApi.getTypeList(limit = 50, offset = 0).results.forEach {
            typesList.add(pokeApi.getType(it.id))
        }

        return@withContext typesList
    }

    /**
     * Returns a list of Pokemons with specified number (limit) and from specified (offset)
     * The 898 ID is hardcoded as the PokeAPI does not contain anymore Pokemon details above that id
     * (using pokemon-details/id despite having generic pokemon information using pokemon/id)
     */
    suspend fun getPokemons(limit: Int, offset: Int): ArrayList<PokemonDto> = withContext(IO) {
        var realLimit = limit
        if (offset + limit > 898) {
            realLimit = 898 - offset
        }

        val pokemonList: ArrayList<PokemonDto> = ArrayList()

        pokeApi.getPokemonList(offset, realLimit).results.forEach {
            val pokemon = pokemonUtils.convertPokemonToDto(pokeApi.getPokemon(it.id))
            pokemon.species = pokeApi.getPokemonSpecies(it.id)
            pokemonList.add(pokemon)
        }
        return@withContext pokemonList
    }

    /**
     * Creates a coroutine that returns every Pokemon contained in the API
     */
    suspend fun getAllPokemons(): ArrayList<PokemonDto> = withContext(IO) {
        val pokemonList: ArrayList<PokemonDto> = ArrayList()
        pokeApi.getPokemonList(0, 898).results.forEach {
            val pokemon = pokemonUtils.convertPokemonToDto(pokeApi.getPokemon(it.id))
            pokemon.species = pokeApi.getPokemonSpecies(it.id)
            pokemonList.add(pokemon)
        }
        return@withContext pokemonList
    }

    /**
     * Creates a coroutine that returns evolution chain from its id
     */
    suspend fun getEvolutionChain(evoChainId: Int): EvolutionChain = withContext(IO) {
        return@withContext pokeApi.getEvolutionChain(evoChainId)
    }
}