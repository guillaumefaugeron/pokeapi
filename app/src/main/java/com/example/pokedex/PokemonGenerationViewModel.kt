package com.example.pokedex

import com.example.pokedex.model.PokemonGeneration

data class PokemonGenerationViewModel(val pokemonGeneration: PokemonGeneration?)