package com.example.pokedex

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.pokedex.model.User
import com.google.gson.Gson
import java.io.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        try {
            readDataArrayFromFileAndMapToObject("user.json", User::class.java)
            val intent = Intent(this, PokeListActivity::class.java)
            startActivity(intent)
        } catch (e: FileNotFoundException) {
            val pokedexLogo: ImageView = findViewById(R.id.pokedexLogo)
            Glide.with(this).load(R.raw.pokedex).into(pokedexLogo)
            findViewById<Button>(R.id.done_button).setOnClickListener {
                val usernameEditText = findViewById<EditText>(R.id.nickname_edit)
                if (TextUtils.isEmpty(usernameEditText.text.toString()) || usernameEditText.text.toString().length > 15) {
                    Toast.makeText(
                        this, "Le nom ne peut pas être vide ou faire plus de 15 caractères.",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    updateNickname(usernameEditText.text.toString())
                    val intent = Intent(it.context, PokeListActivity::class.java)
                    startActivity(intent)
                }
            }
        }
    }

    private fun updateNickname(username: String) {
        if (username.isNotEmpty() && username.length <= 15) {
            try {
                val user = readDataArrayFromFileAndMapToObject("user.json", User::class.java)
                if (user != null) {
                    val newUser = User()
                    newUser.username = username
                    newUser.capturedPokemon = listOf()

                }
            } catch (e: FileNotFoundException) {
                val user = User()
                user.username = username
                user.capturedPokemon = listOf()
                val gson = Gson()
                saveDataToNewFile("user.json", gson.toJson(user))
            }
        }
    }

    private fun saveDataToNewFile(fileName: String, fileContent: String) {
        val fileOutputStream: FileOutputStream

        try {
            fileOutputStream = openFileOutput(fileName, Context.MODE_PRIVATE)
            fileOutputStream.write(fileContent.toByteArray())
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun readDataArrayFromFileAndMapToObject(fileName: String, clazz: Class<*>?): Any? {
        var fileInputStream: FileInputStream? = null
        fileInputStream = openFileInput(fileName)
        val inputStreamReader = InputStreamReader(fileInputStream)
        val bufferedReader = BufferedReader(inputStreamReader)
        val stringBuilder: StringBuilder = StringBuilder()
        var text: String? = null
        while (run {
                text = bufferedReader.readLine()
                text
            } != null) {
            stringBuilder.append(text)
        }
        val data = stringBuilder.toString()
        val gson = Gson()
        return gson.fromJson(data, clazz)
    }
}