package com.example.pokedex.utils

import com.example.pokedex.model.PokemonDto
import me.sargunvohra.lib.pokekotlin.model.Pokemon

// This class contains generic utils used across the application
class PokemonUtils {

    /**
     * Converts a Pokemon object used in PokeKotlin to our PokemonDto class, containing species data
     */
    fun convertPokemonToDto(pokemon: Pokemon): PokemonDto {
        return PokemonDto(
            pokemon.id,
            pokemon.name,
            pokemon.baseExperience,
            pokemon.height,
            pokemon.isDefault,
            pokemon.order,
            pokemon.weight,
            null,
            pokemon.abilities,
            pokemon.forms,
            pokemon.gameIndices,
            pokemon.heldItems,
            pokemon.moves,
            pokemon.stats,
            pokemon.types,
            pokemon.sprites
        )
    }

    /**
     * Returns color code according to Pokemon type
     */
    fun getPokemonTypeColor(pokemonType: String?): String {
        val backgroundColor = when (pokemonType) {
            "normal" -> "#A8A77A"
            "fire" -> "#EE8130"
            "water" -> "#6390F0"
            "electric" -> "#F7D02C"
            "grass" -> "#7AC74C"
            "ice" -> "#96D9D6"
            "fighting" -> "#C22E28"
            "poison" -> "#A33EA1"
            "ground" -> "#E2BF65"
            "flying" -> "#A98FF3"
            "psychic" -> "#F95587"
            "bug" -> "#A6B91A"
            "rock" -> "#B6A136"
            "ghost" -> "#735797"
            "dragon" -> "#6F35FC"
            "dark" -> "#705746"
            "steel" -> "#B7B7CE"
            "fairy" -> "#D685AD"
            else -> {
                "#FFFFFF"
            }
        }
        return backgroundColor
    }
}