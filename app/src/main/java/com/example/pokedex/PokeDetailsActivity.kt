package com.example.pokedex

import android.content.Intent
import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.example.pokedex.model.PokemonDto
import com.example.pokedex.service.PokeService
import com.example.pokedex.utils.PokemonUtils
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet

import com.github.mikephil.charting.data.RadarEntry

import android.util.SparseIntArray

import android.graphics.Typeface

import com.github.mikephil.charting.charts.RadarChart
import com.github.mikephil.charting.animation.Easing

import com.github.mikephil.charting.data.RadarData

import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import me.sargunvohra.lib.pokekotlin.model.EvolutionChain

import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.flexbox.FlexboxLayout
import kotlinx.coroutines.*
import me.sargunvohra.lib.pokekotlin.model.Type
import java.util.concurrent.Executors


class PokeDetailsActivity : AppCompatActivity(), CoroutineScope by MainScope() {
    private val pokeUtils = PokemonUtils()
    private val pokeService = PokeService()
    private lateinit var types: List<Type>
    private var isLoading = true

    private val threadPool = Executors.newSingleThreadExecutor()!!
    private val threadPoolDispatcher = threadPool.asCoroutineDispatcher()

    // Chart
    private var mChart: RadarChart? = null
    private val mTfLight: Typeface? = null
    private val factors = SparseIntArray(6)
    private val scores = SparseIntArray(6)
    private val entries: ArrayList<RadarEntry> = ArrayList()
    private val dataSets: ArrayList<IRadarDataSet> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_poke_details)
        val pokemonId = intent.extras?.get("EXTRA_POKEMON_ID") as Int

        // Handles "shimmer" effect
        val pokemonDetailsContainer = findViewById<LinearLayout>(R.id.pokemonDetailsContainer)
        val shimmerFrameLayout = findViewById<ShimmerFrameLayout>(R.id.shimmerFrameLayout)
        pokemonDetailsContainer.visibility = View.GONE
        shimmerFrameLayout.visibility = View.VISIBLE
        shimmerFrameLayout.startShimmer()

        // Prepares chart
        factors.append(0, R.string.hp)
        factors.append(1, R.string.attack)
        factors.append(2, R.string.defense)
        factors.append(3, R.string.special_attack)
        factors.append(4, R.string.special_defense)
        factors.append(5, R.string.speed)

        mChart = findViewById<View>(R.id.pokemonDetailsChart) as RadarChart
        val xAxis = mChart!!.xAxis
        xAxis.xOffset = 0f
        xAxis.yOffset = 0f
        xAxis.typeface = mTfLight
        xAxis.textSize = 15F
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val mFactors = arrayOf(
                    getString(factors[0]), getString(factors[1]), getString(factors[2]),
                    getString(factors[3]), getString(factors[4]), getString(factors[5])
                )
                return mFactors[value.toInt() % mFactors.size]
            }
        }
        val yAxis = mChart!!.yAxis
        yAxis.axisMinimum = 0F
        yAxis.typeface = mTfLight
        yAxis.textSize = 15F
        yAxis.setLabelCount(6, false)
        yAxis.setDrawLabels(false)
        mChart!!.legend.isEnabled = false
        mChart!!.description.isEnabled = false
        mChart!!.animateXY(
            1400, 1400,
            Easing.EaseInOutQuad,
            Easing.EaseInOutQuad
        )

        // Loads pokemon details in a coroutine, then hides shimmer effect
        launch {
            types = pokeService.getTypes()
            loadPokemonDetails(pokemonId)
            shimmerFrameLayout.stopShimmer()
            isLoading = false
            shimmerFrameLayout.visibility = View.GONE
            pokemonDetailsContainer.visibility = View.VISIBLE
        }
    }

    /**
     * Loads every data required to display Pokemon details, i.e pokemon data and Pokemons in its
     * evolution chain
     */
    private suspend fun loadPokemonDetails(id: Int) = withContext(threadPoolDispatcher) {
        val pokeApi = PokeApiClient()
        val pokemonUtils = PokemonUtils()
        var pokemonDto: PokemonDto
        var evoChain: EvolutionChain

        // Starts a coroutine to load pokemon details from API
        launch {
            val pokemon = pokeApi.getPokemon(id)
            pokemonDto = pokemonUtils.convertPokemonToDto(pokemon)
            pokemonDto.species = pokeApi.getPokemonSpecies(id)
            evoChain = pokeService.getEvolutionChain(pokemonDto.species!!.evolutionChain.id)
            val evoChainPokemons: ArrayList<PokemonDto> = ArrayList()
            evoChainPokemons.add(pokeService.getPokemon(evoChain.chain.species.id))

            // If pokemon has evolutions (before or after)
            if (evoChain.chain.evolvesTo.isNotEmpty()) {
                var currentEvolvesToList = evoChain.chain.evolvesTo
                // We loop for each Pokemon in the evolution and we load their data
                while (currentEvolvesToList.isNotEmpty()) {
                    evoChainPokemons.add(pokeService.getPokemon(currentEvolvesToList[0].species.id))
                    currentEvolvesToList = currentEvolvesToList[0].evolvesTo
                }
            }

            // Adds Pokemon name
            findViewById<TextView>(R.id.pokemonDetailsName).text =
                pokemonDto.species?.names?.first { it.language.id == 5 }?.name

            // Adds Pokemon description
            findViewById<TextView>(R.id.pokemonDetailsDescription).text =
                pokemonDto.species?.flavorTextEntries?.first { it.language.id == 5 }?.flavorText

            // This block handles display of type(s), with proper text and color
            val pokemonType0 = pokemonDto.types[0].type.name
            var pokemonType1 = ""
            try {
                pokemonType1 = pokemonDto.types[1].type.name
            } catch (e: IndexOutOfBoundsException) {
            }

            val pokemonType0TextView = findViewById<TextView>(R.id.pokemonDetailsType0)
            val pokemonType1TextView = findViewById<TextView>(R.id.pokemonDetailsType1)

            pokemonType0TextView.text =
                types.first { it.name == pokemonType0 }.names.first { it.language.id == 5 }.name
            pokemonType0TextView.background.colorFilter =
                BlendModeColorFilter(
                    Color.parseColor(pokeUtils.getPokemonTypeColor(pokemonType0)),
                    BlendMode.SRC_ATOP
                )


            // If Pokemon has multiple types
            if (pokemonType1 != "") {
                pokemonType1TextView.visibility = View.VISIBLE
                pokemonType1TextView.text =
                    types.first { it.name == pokemonType1 }.names.first { it.language.id == 5 }.name
                pokemonType1TextView.background.colorFilter =
                    BlendModeColorFilter(
                        Color.parseColor(pokeUtils.getPokemonTypeColor(pokemonType1)),
                        BlendMode.SRC_ATOP
                    )
            } else {
                pokemonType1TextView.visibility = View.INVISIBLE
            }

            // Glide needs to run in UI Thread explicitly
            this@PokeDetailsActivity.runOnUiThread {
                // We need to check if Activity is not finishing (i.e the user is not pressing
                // back button, or it may crash the app.
                if (!this@PokeDetailsActivity.isFinishing) {
                    Glide.with(this@PokeDetailsActivity).load(pokemonDto.sprites.frontDefault)
                        .into(findViewById(R.id.pokemonDetailsImage))
                }
            }


            // This block adds data to the table, weight, height and "genera"
            findViewById<TextView>(R.id.pokemonDetailsHeight).text =
                (pokemon.height.toFloat().div(10)).toString().plus("m")

            findViewById<TextView>(R.id.pokemonDetailsGenera).text =
                (pokemonDto.species?.genera?.first { it.language.id == 5 }?.genus)

            findViewById<TextView>(R.id.pokemonDetailsWeight).text =
                (pokemon.weight.toFloat().div(10)).toString().plus("kg")


            // Appends stats values to the graph
            scores.append(0, pokemon.stats[0].baseStat)
            scores.append(1, pokemon.stats[1].baseStat)
            scores.append(2, pokemon.stats[2].baseStat)
            scores.append(3, pokemon.stats[3].baseStat)
            scores.append(4, pokemon.stats[4].baseStat)
            scores.append(5, pokemon.stats[5].baseStat)

            // Displays chart
            drawChart()

            // We loop through the Pokemons data we gathered before to display them properly at
            // the bottom of the activity
            evoChainPokemons.forEach { it ->
                // We set a variable with current Pokemon data here as we'll use it in
                // another scope later, and "it" will not contain our data.
                val currentPokemonDto: PokemonDto = it

                // Generates layout programmatically
                val linearLayoutContainer = LinearLayout(this@PokeDetailsActivity)
                val pokemonTextView = TextView(this@PokeDetailsActivity)
                val pokemonImageView = ImageView(this@PokeDetailsActivity)
                val layoutSize =
                    this@PokeDetailsActivity.resources.getDimensionPixelSize(R.dimen.evo_chain_size)
                val linearLayoutParams: FlexboxLayout.LayoutParams =
                    FlexboxLayout.LayoutParams(
                        layoutSize,
                        FlexboxLayout.LayoutParams.MATCH_PARENT
                    )
                linearLayoutContainer.layoutParams = linearLayoutParams
                linearLayoutContainer.orientation = LinearLayout.VERTICAL
                val evoChainLayout =
                    findViewById<FlexboxLayout>(R.id.pokemonDetailsEvoChainLayout)

                val imageSize =
                    this@PokeDetailsActivity.resources.getDimensionPixelSize(R.dimen.evo_chain_image_size)
                val imageParams: LinearLayout.LayoutParams =
                    LinearLayout.LayoutParams(imageSize, imageSize)
                pokemonImageView.layoutParams = imageParams

                pokemonTextView.text = it.species?.names?.first { it.language.id == 5 }?.name
                pokemonTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER

                // We add a listener to load details page when clicking on a pokemon in evo chain
                linearLayoutContainer.setOnClickListener {
                    val intent =
                        Intent(this@PokeDetailsActivity, PokeDetailsActivity::class.java).apply {
                            putExtra("EXTRA_POKEMON_ID", currentPokemonDto.id)
                        }
                    startActivity(intent)
                }

                linearLayoutContainer.addView(pokemonImageView)
                linearLayoutContainer.addView(pokemonTextView)

                this@PokeDetailsActivity.runOnUiThread {
                    evoChainLayout.addView(linearLayoutContainer)
                }


                // If the current pokemon is not the last of the chain, we add a chevron for
                // better visuals
                if (evoChainPokemons.indexOf(it) < evoChainPokemons.size - 1) {
                    val chevronImageView = ImageView(this@PokeDetailsActivity)
                    val chevronSize =
                        this@PokeDetailsActivity.resources.getDimensionPixelSize(R.dimen.evo_chain_chevron_size)
                    chevronImageView.setImageResource(R.drawable.ic_baseline_chevron_right_24)
                    val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                        chevronSize,
                        chevronSize
                    )
                    params.setMargins(0, 35, 0, 0)
                    chevronImageView.layoutParams = params

                    this@PokeDetailsActivity.runOnUiThread { evoChainLayout.addView(chevronImageView) }
                }
                this@PokeDetailsActivity.runOnUiThread {
                    if (!this@PokeDetailsActivity.isFinishing) {
                        Glide.with(this@PokeDetailsActivity).load(it.sprites.frontDefault)
                            .into(pokemonImageView)
                    }
                }

            }
        }
    }

    /**
     * Handles the creation and display of the radar chart with pokemon stats
     */
    private fun drawChart() {
        entries.clear()
        for (i in 0..5) {
            entries.add(RadarEntry(scores[i].toFloat()))
        }
        val dataSet = RadarDataSet(entries, "")
        dataSet.color = R.color.black
        dataSet.setDrawFilled(true)
        dataSets.add(dataSet)
        val data = RadarData(dataSets)
        data.setValueTypeface(mTfLight)
        data.setValueTextSize(15f)
        val vf: ValueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return "" + value.toInt()
            }
        }
        data.setValueFormatter(vf)
        mChart = findViewById<View>(R.id.pokemonDetailsChart) as RadarChart
        mChart!!.data = data
        mChart!!.invalidate()
    }
}