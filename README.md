# Projet Pokédex Kotlin

Projet d'application mobile réalisé en Kotlin permettant d'afficher la liste des différents  Pokémons, leurs informations et de les noter comme étant capturé.

## Membres du groupe

 - FAUGERON Guillaume
 - ARMENOULT Florian

## Documentation
La documentation du projet contenant la liste des fonctionnalités, des captures d'écrans la justification des choix techniques se trouvent dans le fichier PDF à la racine du projet.

## Lancement du projet
Avec Android Studio, bien installé les dépendances du fichier build.gradle avant de build l'application.
